import * as React from 'react'
import { Paper, Typography } from '@material-ui/core';
import {connect} from 'react-redux'


interface IRelatorioCompraProps extends React.Props<any>  {
    ReducerCarrinho: Array<object>,
    ReducerValor: number,
}

class RelatorioCompra extends React.Component<IRelatorioCompraProps> {
      
    public styleContainerCarrinho: React.CSSProperties = {
        width: '98%',
        margin: '0 auto',
        padding: 15,
        animation: 'fadein 2s', /* Safari, Chrome and Opera > 12.1 */
    }
    public styleTituloCarrinho: React.CSSProperties  = {
        fontSize: 20,
        margin: '0 auto'
    }
    public styleItensCarrinho: React.CSSProperties = {
        fontSize: 15,
        alignItems: 'start'
    }

    public styleItensComprados: React.CSSProperties = {
        padding: 10,
        margin: 10,
        fontSize: 15
    }

    public CalcularDesconto()
    {
      const qtdeLivrosUnicos = new Set(this.props.ReducerCarrinho).size
              switch (qtdeLivrosUnicos) {
              case 0:
                  {
                  return 0
                  }
              case 1:
                  {
                  return 0
                  }
              case 2:
                  {
                  return 0.05
                  }
              case 3:
                  {
                  return 0.1
                  }
              case 4:
                  {
                  return 0.2
                  }
              default:
                  {
                  return 0.25
                  }
              }
    }

    public render()
    {
        const desconto: number = this.CalcularDesconto()
        const ItensCompra = this.props.ReducerCarrinho.map((obj: any,indice: number) => {
            return (
                <div key={indice}>
                    {obj['TituloLivro']}
                </div>
            )
        })

        return(
            <Paper style={this.styleContainerCarrinho}>
                <Typography style={this.styleTituloCarrinho}>
                    Carrinho:
                </Typography>
                <div style={this.styleItensComprados}>
                    {ItensCompra}
                </div>
                <Typography>
                    Desconto: {desconto*100} %
                </Typography>
                <Typography style={this.styleItensCarrinho}>
                    Valor total: <span style={{textDecoration: 'line-through'}}>R${this.props.ReducerValor.toFixed(2)}</span> R${(this.props.ReducerValor * (1 - desconto)).toFixed(2)}
                </Typography>
            </Paper>
        )
    }
}

const MapStateToProps = (store: any) => ({
    ReducerCarrinho: store.ReducerCarrinho,
    ReducerValor: store.ReducerValor
});

export default connect(MapStateToProps)(RelatorioCompra);