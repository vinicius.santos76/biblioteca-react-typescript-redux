import * as React from 'react';
import * as DadosBiblioteca from './DadosBiblioteca.json';
import Livro from './Livro'

const styleContainer: React.CSSProperties = {
  display: 'flex',
  width: '100%',
  marginTop: 30
}

export default class ContainerLivros extends React.Component {

  public itensLivros = DadosBiblioteca.livros.map((itens: object, i: number) => {
    return (
      <div key={i} >
        <Livro ObjLivro={itens} TituloLivro={itens["TituloLivro"]} Autor={itens["Autor"]} Imagem={itens["Imagem"]} Valor={itens["Valor"]} Conteudo={itens["Conteudo"]}/>
      </div>
  
    )
  });

  public render() {

    return (
      <div style={styleContainer}>
        {this.itensLivros}
      </div>
    );
  }

}
