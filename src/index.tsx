import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux'
import Store from './store'

ReactDOM.render(
  // Adiciona Driver Provider para que os filhos possam acessar a Store
  <Provider store={Store}> 
    <App />
  </Provider>
  ,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
