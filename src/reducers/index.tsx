import { combineReducers } from 'redux';

import ReducerCarrinho from './ReducerCarrinho';
import ReducerValor from './ReducerValor';

export default combineReducers({
  ReducerCarrinho,
  ReducerValor
});
